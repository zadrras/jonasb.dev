FROM haskell:8.4

WORKDIR build
ADD jonasb-dev.cabal .
RUN stack init
RUN stack install --only-dependencies

ADD . .
RUN stack build
CMD stack exec site build

